package chernovol.component;

import chernovol.entity.User;

public class SecurityContext {
    private static final ThreadLocal<User> userThread = new ThreadLocal<>();
    public static User get() {
        return userThread.get();
    }
    public static void set(User user) {
        userThread.set(user);
    }
}
