package chernovol.database;

import chernovol.entity.Note;
import chernovol.exception.NoteException;

import java.util.ArrayList;
import java.util.List;
//Класс базы даных заметок, сделан по паттерну Singletone (Замена реальной БД)
public class NotesDB {
    private static NotesDB notesDB;
    private final List<Note> notes;

    private NotesDB() {
        this.notes = new ArrayList<>();
    }

    public void addNote(Note note){
        if(isPresent(note)){
            throw new NoteException("Данное сочетание названия и текста заметки уже существует");
        }
        notes.add(note);
    }

    public void deleteNote(Note note){
        if(!isPresent(note)){
            throw new NoteException("Данного сочетания названия и текста заметки не существует");
        }
        notes.remove(getNote(note.getNoteName(),note.getNoteText()));
    }

    public void updateNote(Note oldNote, Note newNote){
        if(!isPresent(oldNote)){
            throw new NoteException("Обновляемой заметки не существует");
        } else if (isPresent(newNote)){
            throw new NoteException("Обнавленная заметка уже существует");
        }
        int index = notes.indexOf(getNote(oldNote.getNoteName(),oldNote.getNoteText()));
        notes.remove(index);
        notes.add(index,newNote);
    }
    //Вспомогательный метод возвращает заметку по сочетанию Названия и Текста заметки
    private Note getNote(String noteName, String noteText){
        return notes.stream()
                .filter(note -> note.getNoteName().equals(noteName))
                .filter(note -> note.getNoteText().equals(noteText)).findFirst().get();
    }
    //Метод для проверки наличия в базе сочетания Имени и Текста заметки
    private boolean isPresent(Note note){
        return notes.stream()
                .filter(n -> n.getNoteName().equals(note.getNoteName()))
                .anyMatch(n -> n.getNoteText().equals(note.getNoteText()));
    }

    //Метод возвращает ссылку на базу заметок
    public static NotesDB getNotesDB(){
        NotesDB result = notesDB;
        if(result != null){
            return result;
        }
        synchronized (UsersDB.class){
            if (notesDB == null){
                notesDB = new NotesDB();
            }
            return notesDB;
        }
    }

    public List<Note> getNotes() {
        return notes;
    }
}
