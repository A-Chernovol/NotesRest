package chernovol.database;

import chernovol.dto.SignInDto;
import chernovol.entity.User;
import chernovol.exception.UserException;

import java.util.ArrayList;
import java.util.List;
//Класс база данных пользователей, сделан по паттерну Singletone (Замена реальной БД)
public class UsersDB {
    private  static UsersDB usersDB;
    private final List<User> users ;

    private UsersDB() {
        this.users = new ArrayList<>();
    }

    public void addUser(User user){
        if(isExist(user.getLogin())){
            throw new UserException("Такой пользователь уже существует");
        }
        this.users.add(user);
    }

    public  User signIn(SignInDto dto){
        if(!isExist(dto.getLogin())){
            throw new UserException("Такого пользователя не существует");
        }
        if(!checkPassword(dto)){
            throw new UserException("Неверный пароль");
        }
        return users.stream().filter(user -> user.getLogin().equals(dto.getLogin())).filter(pass -> pass.getPassword().equals(dto.getPassword())).findFirst().get();
    }
    public  User getByLogin(String login){
        if(!isExist(login)){
            throw new UserException("Такого пользователя не существует");
        }
        return users.stream().filter(user -> user.getLogin().equals(login)).findFirst().get();
    }

    //Вспомогательный метод, проверяет существует ли логин в БД
    private boolean isExist(String login){
        return users.stream().anyMatch(u -> u.getLogin().equals(login));
    }
    private boolean checkPassword(SignInDto dto){
        return users.stream().filter(user -> user.getLogin().equals(dto.getLogin())).anyMatch(pass -> pass.getPassword().equals(dto.getPassword()));
    }

    public static UsersDB getUsersDB(){
        UsersDB result = usersDB;
        if(result != null){
            return result;
        }
        synchronized (UsersDB.class){
            if (usersDB == null){
                usersDB = new UsersDB();
            }
            return usersDB;
        }
    }
}
