package chernovol.dto;

public class UpdateNoteDto {
    private String oldNoteName;
    private String oldNoteText;
    private String newNoteName;
    private String newNoteText;

    public String getOldNoteName() {
        return oldNoteName;
    }

    public void setOldNoteName(String oldNoteName) {
        this.oldNoteName = oldNoteName;
    }

    public String getOldNoteText() {
        return oldNoteText;
    }

    public void setOldNoteText(String oldNoteText) {
        this.oldNoteText = oldNoteText;
    }

    public String getNewNoteName() {
        return newNoteName;
    }

    public void setNewNoteName(String newNoteName) {
        this.newNoteName = newNoteName;
    }

    public String getNewNoteText() {
        return newNoteText;
    }

    public void setNewNoteText(String newNoteText) {
        this.newNoteText = newNoteText;
    }
}
