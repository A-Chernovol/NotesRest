package chernovol.entity;

public class Note {
    private final User user;
    private final String noteText;
    private final String noteName;

    public String getNoteName() {
        return noteName;
    }

    public Note(User user, String noteText, String noteName) {
        this.user = user;
        this.noteText = noteText;
        this.noteName = noteName;
    }

    public User getUser() {
        return user;
    }

    public String getNoteText() {
        return noteText;
    }
}
