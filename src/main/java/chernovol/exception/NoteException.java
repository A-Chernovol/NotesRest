package chernovol.exception;

public class NoteException extends RuntimeException{
    public NoteException(String message) {
        super(message);
    }
}
