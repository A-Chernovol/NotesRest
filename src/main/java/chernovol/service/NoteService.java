package chernovol.service;

import chernovol.database.NotesDB;
import chernovol.entity.Note;
import chernovol.entity.User;

public class NoteService {
    NotesDB notesDB = NotesDB.getNotesDB();

    public void addNote(User user, String noteText, String noteName){
        notesDB.addNote(new Note(user,noteText, noteName));
    }

    public void deleteNote(Note note){
        notesDB.deleteNote(note);
    }

    public String showNotes(User user){
        StringBuilder sb = new StringBuilder();
        sb.append(user.getLogin()).append('\n');
        if(!notesDB.getNotes().isEmpty()){
            for(Note n : notesDB.getNotes()){
                if(n.getUser().getLogin().equals(user.getLogin())){
                    sb.append('\t').append('<').append(n.getNoteName()).append('>').append(n.getNoteText()).append('\n');
                }
            }
        } else {
            sb.append('\t').append("У пользователя нет заметок");
        }
        return sb.toString();
    }

    public void updateNote(Note oldNote, Note newNote) {
        notesDB.updateNote(oldNote,newNote);
    }
}
