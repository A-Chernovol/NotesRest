package chernovol.service;

import chernovol.database.UsersDB;
import chernovol.dto.SignInDto;
import chernovol.entity.User;

public class SignInService {
    UsersDB usersDB = UsersDB.getUsersDB();

    public User signIn(SignInDto dto){
        return usersDB.signIn(dto);
    }
    public User getByLogin(String login){
        return usersDB.getByLogin(login);
    }
}
