package chernovol.service;

import chernovol.database.UsersDB;
import chernovol.entity.User;

public class SignUpService {
    UsersDB usersDB = UsersDB.getUsersDB();

    public User addUser(User user){
        usersDB.addUser(user);
        return user;
    }
}
