package chernovol.validator;

import chernovol.dto.NoteDto;
import chernovol.exception.NoteException;

public class DeleteNoteValidator implements Validator<NoteDto> {
    @Override
    public void validate(NoteDto dto) {
        if (dto.getNoteName() == null || dto.getNoteName().isEmpty()) {
            throw new NoteException("Введите имя удаляемой заметки");
        }

        if (dto.getNoteText() == null || dto.getNoteText().isEmpty()) {
            throw new NoteException("Введите текст удаляемой заметки");
        }
    }
}
