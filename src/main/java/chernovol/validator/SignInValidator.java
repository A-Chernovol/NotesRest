package chernovol.validator;

import chernovol.dto.SignInDto;
import chernovol.exception.UserException;

public class SignInValidator implements Validator<SignInDto> {
    @Override
    public void validate(SignInDto dto) {
        if (dto.getLogin() == null || dto.getLogin().isEmpty()) {
            throw new UserException("Введите логин");
        }

        if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
            throw new UserException("Введите пароль");
        }
    }
}
