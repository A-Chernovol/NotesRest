package chernovol.validator;
import chernovol.dto.SignUpDto;
import chernovol.exception.UserException;

public class SignUpValidator implements Validator<SignUpDto>{
    @Override
    public void validate(SignUpDto dto) {
        if (dto.getLogin() == null || dto.getLogin().isEmpty()) {
            throw new UserException("Введите логин");
        }

        if (dto.getPassword() == null || dto.getPassword().isEmpty()) {
            throw new UserException("Введите пароль");
        }
    }
}
