package chernovol.validator;

import chernovol.dto.UpdateNoteDto;
import chernovol.exception.NoteException;

public class UpdateNoteValidator implements Validator<UpdateNoteDto>{
    @Override
    public void validate(UpdateNoteDto dto) {
        if (dto.getOldNoteName() == null || dto.getOldNoteName().isEmpty()) {
            throw new NoteException("Введите имя обновляемой заметки");
        }

        if (dto.getOldNoteText() == null || dto.getOldNoteText().isEmpty()) {
            throw new NoteException("Введите текст обновляемой заметки");
        }

        if (dto.getNewNoteName() == null || dto.getNewNoteName().isEmpty()) {
            throw new NoteException("Введите имя новой заметки");
        }

        if (dto.getNewNoteText() == null || dto.getNewNoteText().isEmpty()) {
            throw new NoteException("Введите текст новой заметки");
        }
    }
}
