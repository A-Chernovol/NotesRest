package chernovol.validator;


public interface Validator<T> {
    void validate(T dto);
}
