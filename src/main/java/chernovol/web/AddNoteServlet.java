package chernovol.web;

import chernovol.component.SecurityContext;
import chernovol.dto.NoteDto;
import chernovol.dto.ResponseDto;
import chernovol.exception.NoteException;
import chernovol.service.NoteService;
import chernovol.validator.AddNoteValidator;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "AddNoteServlet", value = "/addnote")
public class AddNoteServlet extends AbstractServlet {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final NoteService noteService = new NoteService();
    private final AddNoteValidator addNoteValidator = new AddNoteValidator();
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        NoteDto noteDto = this.objectMapper.readValue(request.getInputStream(), NoteDto.class);

        try {

            addNoteValidator.validate(noteDto);
            noteService.addNote(SecurityContext.get(),noteDto.getNoteText(),noteDto.getNoteName());

            response(response,200,this.objectMapper.writeValueAsString("added"));
        }catch (NoteException e){
            response(response,400,this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));
        }
    }
}
