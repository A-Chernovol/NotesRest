package chernovol.web;

import chernovol.component.SecurityContext;
import chernovol.dto.NoteDto;
import chernovol.dto.ResponseDto;
import chernovol.entity.Note;
import chernovol.exception.NoteException;
import chernovol.service.NoteService;
import chernovol.validator.DeleteNoteValidator;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "DeleteNoteServlet", value = "/deletenote")
public class DeleteNoteServlet extends AbstractServlet {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final NoteService noteService = new NoteService();
    private final DeleteNoteValidator deleteNoteValidator = new DeleteNoteValidator();
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        NoteDto noteDto = this.objectMapper.readValue(request.getInputStream(), NoteDto.class);
        try{

            deleteNoteValidator.validate(noteDto);
            noteService.deleteNote(new Note(SecurityContext.get(),noteDto.getNoteText(),noteDto.getNoteName()));

            response(response,200,this.objectMapper.writeValueAsString("deleted"));

        } catch (NoteException e){
            response(response,400,this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));
        }
    }
}
