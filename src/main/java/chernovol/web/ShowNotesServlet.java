package chernovol.web;

import chernovol.component.SecurityContext;
import chernovol.service.NoteService;

import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "ShowNotesServlet", value = "/shownotes")
public class ShowNotesServlet extends AbstractServlet {
    private final NoteService noteService = new NoteService();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setCharacterEncoding("UTF-8");
        response.getWriter().println(noteService.showNotes(SecurityContext.get()));
        response.getWriter().flush();
    }

}
