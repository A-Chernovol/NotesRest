package chernovol.web;

import chernovol.dto.ResponseDto;
import chernovol.dto.SignInDto;
import chernovol.dto.TokenDto;
import chernovol.entity.User;
import chernovol.exception.UserException;
import chernovol.component.JwtToken;
import chernovol.service.SignInService;
import chernovol.validator.SignInValidator;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "SignInServlet", value = "/signin")
public class SignInServlet extends AbstractServlet {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final SignInService signInService = new SignInService();
    private final SignInValidator signInValidator = new SignInValidator();
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        SignInDto signInDto = this.objectMapper.readValue(request.getInputStream(), SignInDto.class);

        try {
            signInValidator.validate(signInDto);

            User user = this.signInService.signIn(signInDto);
            String token = JwtToken.getToken(user);

            response(response,200,this.objectMapper.writeValueAsString(new TokenDto(token)));
        } catch (UserException e) {
            response(response,400,this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));
        }
    }
}
