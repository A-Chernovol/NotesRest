package chernovol.web;

import chernovol.dto.ResponseDto;
import chernovol.dto.SignUpDto;
import chernovol.dto.TokenDto;
import chernovol.entity.User;
import chernovol.exception.UserException;
import chernovol.component.JwtToken;
import chernovol.service.SignUpService;
import chernovol.validator.SignUpValidator;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "SignUpServlet", value = "/signup")
public class SignUpServlet extends AbstractServlet {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final SignUpService signUpService = new SignUpService();
    private final SignUpValidator signUpValidator = new SignUpValidator();
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        SignUpDto signUpDto = this.objectMapper.readValue(request.getInputStream(), SignUpDto.class);

        try {
            signUpValidator.validate(signUpDto);

            User user = this.signUpService.addUser(new User(
                    signUpDto.getLogin(),
                    signUpDto.getPassword(),
                    signUpDto.getFirstName(),
                    signUpDto.getLastName()));

            String token = JwtToken.getToken(user);

            response(response,200,this.objectMapper.writeValueAsString(new TokenDto(token)));
        } catch (UserException e){
            response(response, 400,this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));
        }
    }
}

