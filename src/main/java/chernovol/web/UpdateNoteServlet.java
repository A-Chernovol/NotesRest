package chernovol.web;

import chernovol.component.SecurityContext;
import chernovol.dto.ResponseDto;
import chernovol.dto.UpdateNoteDto;
import chernovol.entity.Note;
import chernovol.exception.NoteException;
import chernovol.service.NoteService;
import chernovol.validator.UpdateNoteValidator;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "UpdateNoteServlet", value = "/updatenote")
public class UpdateNoteServlet extends AbstractServlet {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final NoteService noteService = new NoteService();
    private final UpdateNoteValidator updateNoteValidator = new UpdateNoteValidator();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        UpdateNoteDto updateNoteDto = this.objectMapper.readValue(request.getInputStream(), UpdateNoteDto.class);

        try {
            updateNoteValidator.validate(updateNoteDto);
            noteService.updateNote(
                    new Note(SecurityContext.get(),updateNoteDto.getOldNoteText(),updateNoteDto.getOldNoteName()),
                    new Note(SecurityContext.get(),updateNoteDto.getNewNoteText(),updateNoteDto.getNewNoteName()));

            response(response,200,this.objectMapper.writeValueAsString("updated"));

        } catch (NoteException e){
            response(response,401,this.objectMapper.writeValueAsString(new ResponseDto(e.getMessage())));
        }
    }
}
