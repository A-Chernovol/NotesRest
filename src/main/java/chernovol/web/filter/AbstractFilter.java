package chernovol.web.filter;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


abstract class AbstractFilter{
    public void response(HttpServletResponse response, int statusCode, String message) throws IOException {
        response.setStatus(statusCode);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(message);
        response.getWriter().flush();
    }
}
