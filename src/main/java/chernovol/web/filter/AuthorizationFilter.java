package chernovol.web.filter;

import chernovol.component.SecurityContext;
import chernovol.dto.ResponseDto;
import chernovol.component.JwtToken;
import chernovol.service.SignInService;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtParser;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebFilter(filterName = "AuthorizationFilter", urlPatterns = "/*")
public class AuthorizationFilter extends AbstractFilter implements Filter {
    private JwtParser jwtParser;
    private final SignInService signInService = new SignInService();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final List<String> PROTECTED_PATHS = new ArrayList<>();

    @Override
    public void init(FilterConfig filterConfig) {
        this.jwtParser = JwtToken.getParser();

        PROTECTED_PATHS.add("/addnote");
        PROTECTED_PATHS.add("/deletenote");
        PROTECTED_PATHS.add("/updatenote");
        PROTECTED_PATHS.add("/shownotes");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        String requestUri = req.getRequestURI().replace(req.getContextPath(), "");

        if (PROTECTED_PATHS.stream().anyMatch(url -> url.startsWith(requestUri))) {

            String signHeader = req.getHeader("Authorization");

            if (signHeader == null || signHeader.isEmpty()) {
                response(resp, 401, this.objectMapper.writeValueAsString(new ResponseDto("Токен отсутствует")));
                return;
            }

            String jwtToken = signHeader.replace("Bearer ", "");
            Claims claims;
            try {
                claims = this.jwtParser.parseClaimsJws(jwtToken).getBody();
            } catch (ExpiredJwtException e) {
                response(resp, 401, this.objectMapper.writeValueAsString(new ResponseDto("Токен истёк")));
                return;
            }

            assert claims != null;
            if (signInService.getByLogin(claims.getSubject()) == null) {
                response(resp, 401, this.objectMapper.writeValueAsString(new ResponseDto("Токен не такой")));
                return;
            }

            SecurityContext.set(signInService.getByLogin(claims.getSubject()));
            chain.doFilter(request, response);

        } else {
            chain.doFilter(request, response);
        }
    }
}
